package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

public class CarTable {
	
	private static HashTableOA<Long, Car> cTable = new HashTableOA<Long, Car>(new LongComparator(), new CarComparator());

	public static HashTableOA<Long, Car> getInstance() {
		return cTable;
	}

	//returns new instance of HTOA
	public static void resetCars(){
		cTable = new HashTableOA<Long, Car>(new LongComparator(), new CarComparator());
	}

}
