package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.List;

public class HashTableOA<K, V> implements Map<K, V> {

	public static enum Status{
		IN_USE, EMPTY, USED;
	}

	public static class MapEntry<K,V> {
		private K key;
		private V value;
		private Status status;

		public Status getStatus() {
			return status;
		}
		public void setStatus(Status status) {
			this.status = status;
		}
		public K getKey() {
			return key;
		}
		public void setKey(K key) {
			this.key = key;
		}
		public V getValue() {
			return value;
		}
		public void setValue(V value) {
			this.value = value;
		}
		public MapEntry(K key, V value) {
			super();
			this.key = key;
			this.value = value;
			this.setStatus(Status.IN_USE);
		}

		public MapEntry() {
			super();
			this.key = null;
			this.value = null;
			this.setStatus(Status.EMPTY);
		}	

	}

	private int currentSize;
	private Object[] buckets;
	private static final int DEFAULT_BUCKETS = 10;
	private Comparator<K> keyComp;
	private Comparator<V> vComp;


	public HashTableOA(int numBuckets, Comparator<K> c1, Comparator<V> c2) {
		this.currentSize  = 0;
		this.buckets = new Object[numBuckets];
		this.keyComp = c1;
		this.vComp = c2;
		for (int i =0; i < numBuckets; ++i) {
			this.buckets[i] = new MapEntry<K,V>();
		}

	}

	public HashTableOA(Comparator<K> c1, Comparator<V> c2) {
		this.currentSize = 0;
		this.buckets = new Object[DEFAULT_BUCKETS];
		this.keyComp = c1;
		this.vComp = c2;
		for (int i =0; i < DEFAULT_BUCKETS; ++i) {
			this.buckets[i] = new MapEntry<K,V>();
		}
	}

	//first hash function
	private int hashFunction(K key) {
		int position = key.hashCode()*251;
		position = (position << 32) ;		
		return Math.abs(position % this.buckets.length);
	}

	//second hash function
	private int doubleHash(K key) {
		return Math.abs((((this.hashFunction(key) + 29)^2) % this.buckets.length));
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize == 0;
	}


	/*for each case I'm checking if the keys are the same before
	 * moving on to the next case until I hit linear probing
	 */
	@Override
	public V get(K key) {
		if(this.isEmpty()) return null;
		else {
			int position = this.hashFunction(key);
			MapEntry<K,V> bucket = (MapEntry<K, V>)this.buckets[position];

			//first hash
			if(bucket.getKey()==null) {
				return null;
			}
			else if(this.keyComp.compare(key, bucket.getKey()) == 0) {
				return bucket.getValue();
			}
			//second hash
			else {
				int newPosition = this.doubleHash(key);
				bucket = (MapEntry<K, V>)this.buckets[newPosition];

				if(bucket.getKey() == null) {
					return null;
				}
				else if(this.keyComp.compare(key, bucket.getKey()) == 0) {
					return bucket.getValue();
				}
				//linear probe
				else {

					int count = 0;
					while(bucket.getStatus() != Status.EMPTY) {				
						if(this.keyComp.compare(key, bucket.getKey()) == 0) return bucket.getValue();
						else{
							count++;
							bucket = (MapEntry<K, V>)this.buckets[(newPosition + count)%buckets.length];
						}
					}
					return null;				
				}
			}
		}
	}


	/*for each case I'm first assuming an empty bucket and return null, 
	if it isn't empty I then check if the keys are the same, 
	if true I return the old value, if false I proceed to the next method all the way
	until linear probing
	 */
	@Override
	public V put(K key, V value) {
		if(this.size()==this.buckets.length*.7) {
			this.reAllocate();
		}
		int position = this.hashFunction(key);
		MapEntry<K,V> bucket = (MapEntry<K, V>)this.buckets[position];

		//first hash
		if(bucket.getStatus() != Status.IN_USE) {
			bucket.setKey(key); bucket.setValue(value); bucket.setStatus(Status.IN_USE); this.currentSize++;

			return null;
		}
		else if(this.keyComp.compare(key, bucket.getKey()) == 0) {
			V result = bucket.getValue();
			bucket.setKey(key); bucket.setValue(value); bucket.setStatus(Status.IN_USE); 
			return result;
		}
		//second hash
		else {
			int newPosition = this.doubleHash(key);
			bucket = (MapEntry<K, V>)this.buckets[newPosition];

			if(bucket.getStatus() != Status.IN_USE) {
				bucket.setKey(key); bucket.setValue(value); bucket.setStatus(Status.IN_USE); this.currentSize++;
				return null;
			}
			else if(this.keyComp.compare(key, bucket.getKey()) == 0) {
				V result = bucket.getValue();
				bucket.setKey(key); bucket.setValue(value); bucket.setStatus(Status.IN_USE); return result;
			}
			//linear probe
			else {
				int count = 0;
				while(bucket.getStatus() == Status.IN_USE) {
					if(this.keyComp.compare(key, bucket.getKey()) == 0) {
						V result = bucket.getValue();
						bucket.setKey(key); bucket.setValue(value); bucket.setStatus(Status.IN_USE); return result;
					}
					else{
						count++;
						bucket = (MapEntry<K, V>)this.buckets[(newPosition + count)%buckets.length];
					}
				}
				bucket.setKey(key); bucket.setValue(value); bucket.setStatus(Status.IN_USE); this.currentSize++;
				return null;				
			}
		}
	}


	private void reAllocate() {
		Object[] newBuckets = new Object[this.buckets.length*2];
		Object[] old = this.buckets;
		
		for (int i =0; i < newBuckets.length; ++i) {
			newBuckets[i] = new MapEntry<K,V>();
		}
		
		this.buckets = newBuckets;
		this.currentSize=0;

		for (int i = 0; i < old.length; i++) {
			if(((MapEntry<K, V>) old[i]).getStatus() == Status.IN_USE) {
				this.put(((MapEntry<K, V>) old[i]).getKey(), ((MapEntry<K, V>) old[i]).getValue());
			}
		}
		
	}


	@Override
	public V remove(K key) {
		if(!this.contains(key)) return null;
		else {
			int position = this.hashFunction(key);
			MapEntry<K,V> bucket = (MapEntry<K, V>)this.buckets[position];

			//first hash		
			if(this.keyComp.compare(key, bucket.getKey()) == 0) {
				V result = bucket.getValue(); 
				bucket.setKey(null); bucket.setValue(null); bucket.setStatus(Status.USED); this.currentSize--;
				return result;
			}
			//second hash
			else {
				int newPosition = this.doubleHash(key);
				bucket = (MapEntry<K, V>)this.buckets[newPosition];

				if(this.keyComp.compare(key, bucket.getKey()) == 0) {
					V result = bucket.getValue(); 
					bucket.setKey(null); bucket.setValue(null); bucket.setStatus(Status.USED); this.currentSize--;
					return result;
				}
				//linear probe
				else {
					int count = 0;
					while(bucket.getStatus() == Status.IN_USE) {					
						if(this.keyComp.compare(key, bucket.getKey()) == 0) {
							V result = bucket.getValue(); 
							bucket.setKey(null); bucket.setValue(null); bucket.setStatus(Status.USED); this.currentSize--;
							return result;							}
						else{
							count++;
							bucket = (MapEntry<K, V>)this.buckets[(newPosition + count)%buckets.length];
						}
					}
					return null;
				}
			}
		}
	}

	//returns true if get returns a non-null value
	@Override
	public boolean contains(K key) {
		return (this.get(key) != null);
	}

	//returns a CSDLL with all the keys in the HTOA
	@Override
	public SortedList<K> getKeys() {
		SortedList<K> keys = new CircularSortedDoublyLinkedList<K>(keyComp);
		if(this.isEmpty()) return keys;
		else {
			for (int i = 0; i < this.buckets.length; i++) {
				if(((MapEntry<K, V>) buckets[i]).getStatus() == Status.IN_USE) 
					keys.add(((MapEntry<K, V>) buckets[i]).getKey());
			}
			return keys;
		}
	}

	//returns a CSDLL with all the values in the HTOA
	@Override
	public SortedList<V> getValues() {
		SortedList<V> values = new CircularSortedDoublyLinkedList<V>(vComp);
		if(this.isEmpty()) return values;
		else {
			for (int i = 0; i < this.buckets.length; i++) {
				if(((MapEntry<K, V>) buckets[i]).getStatus() == Status.IN_USE) 
					values.add(((MapEntry<K, V>) buckets[i]).getValue());
			}
			return values;
		}
	}

}
