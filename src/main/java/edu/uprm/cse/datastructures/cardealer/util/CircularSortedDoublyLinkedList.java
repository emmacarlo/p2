package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{

	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> previous;



		public Node(E element, Node<E> next, Node<E> previous) {
			super();
			this.element = element;
			this.next = next;
			this.previous = previous;
		}

		public Node() {
			this.element = null;
			this.next = null;
			this.previous = null;
		}


		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrevious() {
			return previous;
		}
		public void setPrevious(Node<E> previous) {
			this.previous = previous;
		}

	}

	private Comparator<E> comparator;
	private Node<E> header;
	private int currentSize;

	//Circular Sorted Doubly Linked List Iterator
	private class CSDLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;
		
		public CSDLinkedListIterator() {
			super();
			this.nextNode = (Node<E>) header.getNext();

		}

		@Override
		public boolean hasNext() {
			return nextNode.getElement() != null;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

	}

	public CircularSortedDoublyLinkedList(Comparator<E> comparator) {
		super();
		this.comparator = comparator;
		this.header = new Node<E>(null, header, header);
		this.currentSize = 0;
	}

	@Override
	public Iterator<E> iterator() {		
		return new CSDLinkedListIterator<E>();
	}

	//adds an element e to the list
	@Override
	public boolean add(E e) {

		Node<E> newNode = new Node<E>(e, null, null);
		Node<E> target = this.header.getNext();

		//empty list
		if(this.isEmpty()) {
			header.setNext(newNode);
			header.setPrevious(newNode);
			newNode.setNext(header);
			newNode.setPrevious(header);

			this.currentSize++;
			return true;
		}

		//nonempty list
		else {
			while(target.getNext().getElement()!=null) {
				if(this.comparator.compare(newNode.getElement(), target.getElement()) >= 0) {
					target = target.getNext();
				}
				else if(this.comparator.compare(newNode.getElement(), target.getElement()) < 0) {
					newNode.setNext(target);
					newNode.setPrevious(target.getPrevious());
					target.getPrevious().setNext(newNode);
					target.setPrevious(newNode);

					this.currentSize++;				
					return true;
				}
			}
			newNode.setNext(header);
			newNode.setPrevious(target);
			target.setNext(newNode);
			header.setPrevious(newNode);

			this.currentSize++;
			return true;
		}

	}

	//returns current size of list
	@Override
	public int size() {
		return this.currentSize;
	}

	//returns true if it removes element e from list
	@Override
	public boolean remove(E e) {
		if(this.isEmpty() || !this.contains(e)) return false;
		else {
			Node<E> target = this.header.getNext();

			while(target.getNext().getElement()!=null) {
				if(this.comparator.compare(target.getElement(), e) == 0){
					target.getPrevious().setNext(target.getNext());
					target.getNext().setPrevious(target.getPrevious());

					target.setElement(null); target.setNext(null); target.setPrevious(null);
					this.currentSize--;
					return true;
				}
				else {
					target = target.getNext();
				}
			}		

			if(target.getNext().getElement() == null) {
				target.getPrevious().setNext(target.getNext());
				target.getNext().setPrevious(target.getPrevious());

				target.setElement(null); target.setNext(null); target.setPrevious(null);
				this.currentSize--;
				return true;
			}
			else return false;
		}
	}

	//removes at given index
	@Override
	public boolean remove(int index) {
		if(this.isEmpty()) return false;
		if(index < 0 || index >= this.size()) throw new IndexOutOfBoundsException();
		if(index == 0) return this.remove(this.header.getNext().getElement());

		return this.remove(this.get(index));
	}

	//removes all instances of e
	@Override
	public int removeAll(E e) {
		if(!this.contains(e)) return 0;

		int result = 0;
		while(this.contains(e)) {
			this.remove(this.firstIndex(e));
			result++;
		}
		return result;
	}

	//returns first element in list
	@Override
	public E first() {
		if(this.isEmpty()) return null;
		return this.header.getNext().getElement();
	}

	//returns last element in list
	@Override
	public E last() {
		if(this.isEmpty()) return null;
		return this.header.getPrevious().getElement();
	}

	//returns element at given index
	@Override
	public E get(int index) {
		if(index < 0 || index >= this.size()) throw new IndexOutOfBoundsException();
		else {
			Node<E> target = header.getNext();
			int count = 0;
			while(count < index) {
				target = target.getNext();
				count++;
			}
			return target.getElement();
		}
	}	

	//empties out the list
	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
		this.currentSize = 0;

	}

	//returns true if element e is in the list
	@Override
	public boolean contains(E e) {
		if(this.isEmpty()) return false;
		else{
			Node<E> temp = this.header.getNext();
			while(temp.getNext().getElement()!=null) {
				if(this.comparator.compare(temp.getElement(), e) == 0) {
					return true;
				}
				else {
					temp = temp.getNext();
				}
			}
			if(this.comparator.compare(temp.getElement(), e) == 0) return true;
			else return false;
		}
	}

	//returns true if list is empty
	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	//returns first index of element e
	@Override
	public int firstIndex(E e) {
		if(!this.contains(e)) return -1;

		Node<E> target = this.header.getNext();
		int result = 0;

		while(target.getNext().getElement()!=null) {
			if(this.comparator.compare(target.getElement(), e) == 0) {
				return result;
			}
			else {
				target = target.getNext();
				result++;
			}
		}
		return result;
	}

	//returns last index of element e
	@Override
	public int lastIndex(E e) {
		if(!this.contains(e)) return -1;

		Node<E> target = this.header.getPrevious();
		int result = this.size()-1;

		while(target.getPrevious().getElement()!=null) {
			if(this.comparator.compare(target.getElement(), e) == 0) {
				return result;
			}
			else {
				target = target.getPrevious();
				result--;
			}
		}
		return result;
	}

}
