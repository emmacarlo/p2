package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car c1, Car c2) {
		//Based on brand, model, and options
		//equals
		String car1 = c1.getCarBrand().concat(c1.getCarModel().concat(c1.getCarModelOption()));
		String car2 = c2.getCarBrand().concat(c2.getCarModel().concat(c2.getCarModelOption()));
		
		if(car1.compareTo(car2) > 0) {
			return 1;
		}
		
		//greater than
		else if(car1.compareTo(car2) < 0) {
			return -1;
		}
		
		//less than
		else {
			return 0;
		}
	}

}
