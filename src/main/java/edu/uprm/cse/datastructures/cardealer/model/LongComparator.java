package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class LongComparator implements Comparator<Long> {

	@Override
	public int compare(Long o1, Long o2) {
		if(o1.compareTo(o2) < 0) return -1;
		else if(o1.compareTo(o2)>0) return 1;
		else return 0;
	}

}
