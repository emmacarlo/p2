package edu.uprm.cse.datastructures.cardealer;

import java.util.Arrays;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.model.CarTable;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

@Path("/cars")

public class CarManager {

	private final HashTableOA<Long, Car> hTable = CarTable.getInstance();

	//returns an array of type Car containing all the elements in list
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getCars() {
		CarComparator comp = new CarComparator();
		Car[] result = new Car[hTable.size()];
		CircularSortedDoublyLinkedList<Car> cars = (CircularSortedDoublyLinkedList<Car>) hTable.getValues();

		for (int i = 0; i < result.length; i++) {
			result[i] = cars.get(i);
		}
		Arrays.sort(result, comp);
		return result;
	}

	//returns Car matching the id
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		if(hTable.contains(id)) return hTable.get(id);
		else throw new NotFoundException();	
	}

	//adds a Car to list
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car c) {
		if(!hTable.contains(c.getCarId())) {
			hTable.put(c.getCarId(), c);
			return Response.status(201).build();
		}
		else {
			return Response.status(409).build();
		}
		
	}

	//updates Car matching the id
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car c) {
		if(hTable.contains(c.getCarId())) {
			hTable.put(c.getCarId(), c);
			return Response.status(200).build();
		}
		else {
			return Response.status(404).build();
		}
	}

	//deletes Car with matching id from list
	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCar(@PathParam("id") long id) {

		if(hTable.contains(id)) {
			hTable.remove(id);
			return Response.status(200).build();
		}

		return Response.status(404).build();
	}


}
